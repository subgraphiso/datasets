# Re-split root product train/test set

> 2018-03-31

To better asset the performance of machine learning models, the train-test 
split is re-created for the root products dataset. Under this new scheme,
train set contains all classes, while test set only has 792 classes
(these classes all have more than 60 samples in the dataset). 

- `train_root_products_792.csv`: 368,324 rows, all 1,148 classes
- `test_root_products_792.csv`: 15,840 rows, 792 classes, each has 20 samples
